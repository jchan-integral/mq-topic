package au.com.integral.demo.mqtopic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqTopicApplication {

	public static void main(String[] args) {
		SpringApplication.run(MqTopicApplication.class, args);
	}

}
